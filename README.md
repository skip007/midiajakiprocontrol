# README #

This is a command line tool enabling midi control of an Aja KiPro Digital Video Recording / Playback System. 
It is useful in automation programming for presentations and church services.

### How do I get set up? ###

        Usage: KiProControl <KiPro address> <command>  [<midi input #>(0) [<chan>(13) [<note>(0)]]]
        <command>:
            play - start playing the KiPro at the previously cued point.
            stop - stop playing or recording
            record - record a new segment

            list - list available input and output midi ports
            midi - run forever listining for midi control commands on <midi input #>
                noteon  <chan> <note> velocity 0 - play
                noteon  <chan> <note> velocity 127 - record
                noteoff <chan> <note> velocity x - stop

                sysex.Start - record
                sysex.Continue - play
                sysex.Stop - stop

        <KiProAddress> - local network name or ip address of KiPro device to control

        Examples:
            KiProControl 110.168.2.10 stop
            KiProControl KiPro7 record
            KiProControl KiPro7 play

            KiProControl KiPro7 list
            KiProControl KiPro7 midi 1 13 0

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact